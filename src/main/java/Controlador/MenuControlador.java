/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import ClasesONG.Delegaciones;
import Persistencia.ControladoraPersistencia;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;


public class MenuControlador implements Initializable {

    @FXML
    private TextField nombre;
    @FXML
    private Label name;
    @FXML
    private Label cod;
    @FXML
    private TextField codigo;
    @FXML
    private Label dir;
    @FXML
    private TextField director;
    @FXML
    private Label emp;
    @FXML
    private TextField empleados;
    @FXML
    private Button crearDelegacion;
    @FXML
    private Button modificarDelegacion;
    
    private ObservableList<Delegaciones> delegacion;
    
    Delegaciones delegaciones = new Delegaciones();
    
    ControladoraPersistencia controlador = new ControladoraPersistencia();
    @FXML
    private Button eliminarDelegacion;
    @FXML
    private Button BORRAR;
    @FXML
    private TableView<Delegaciones> tabla;
    @FXML
    private TableColumn columnaNombre;
    @FXML
    private TableColumn columnaCodigo;
    @FXML
    private TableColumn columnaDirector;
    @FXML
    private TableColumn columnaEmpleados;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
        Actualizar();
        
    }    
    
    private void Actualizar(){
        
        delegacion = FXCollections.observableArrayList();
        
        this.columnaNombre.setCellValueFactory(new PropertyValueFactory("nombre"));
        this.columnaCodigo.setCellValueFactory(new PropertyValueFactory("codigodel"));
        this.columnaDirector.setCellValueFactory(new PropertyValueFactory("director"));
        this.columnaEmpleados.setCellValueFactory(new PropertyValueFactory("numempleados"));
        
        List<Delegaciones> dl = controlador.listarDelegaciones();
        
        for(Delegaciones d : dl){
            this.delegacion.add(d);
        }
        
        this.tabla.setItems(delegacion);
    }
    
    private void Comprobar(){
        
        
        
    }

    @FXML
    private void clickCrearDelegacion(ActionEvent event) {
        
        delegaciones.setNombre(nombre.getText());
        delegaciones.setCodigodel(Integer.parseInt(codigo.getText()));
        delegaciones.setDirector(director.getText());
        delegaciones.setNumempleados(Integer.parseInt(empleados.getText()));
        
        
        //Crear delegación
        controlador.crearDelegacion(delegaciones);
        
        
        Actualizar();
        
    }

    @FXML
    private void clickModificarDelegacion(ActionEvent event) {
        
        //delegaciones.setNombre(nombre.getText());
        delegaciones.setCodigodel(Integer.parseInt(codigo.getText()));
        delegaciones.setDirector(director.getText());
        delegaciones.setNumempleados(Integer.parseInt(empleados.getText()));
        
        
        //Crear delegación
        controlador.modificarDelegacion(delegaciones);
        
        Actualizar();
    }

    @FXML
    private void clickEliminarDelegacion(ActionEvent event) {
        
        controlador.eliminarDelegacion(Integer.parseInt(codigo.getText()));
        
        Actualizar();
        clickBorrar(event);
    }

    @FXML
    private void clickBorrar(ActionEvent event) {
        
        nombre.setText("");
        codigo.setText("");
        director.setText("");
        empleados.setText("");
        this.codigo.editableProperty();
        
    }

    @FXML
    private void seleccionar(MouseEvent event) {
        
        Delegaciones delegacion = this.tabla.getSelectionModel().getSelectedItem();
        
        if(delegacion != null){
            
            this.nombre.setText(delegacion.getNombre());
            this.codigo.setText(delegacion.getCodigodel()+"");
            this.director.setText(delegacion.getDirector());
            this.empleados.setText(delegacion.getNumempleados()+"");
            this.codigo.disableProperty();
            
        }
        
    }
    
}
