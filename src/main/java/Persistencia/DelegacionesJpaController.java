package Persistencia;

import ClasesONG.Delegaciones;
import Persistencia.exceptions.NonexistentEntityException;
import Persistencia.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;


public class DelegacionesJpaController implements Serializable {

    public DelegacionesJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;
    
    public DelegacionesJpaController(){
        emf = Persistence.createEntityManagerFactory("aplicacion");
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Delegaciones delegaciones) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(delegaciones);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findDelegaciones(delegaciones.getCodigodel()) != null) {
                throw new PreexistingEntityException("Delegaciones " + delegaciones + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Delegaciones delegaciones) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            delegaciones = em.merge(delegaciones);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                int id = delegaciones.getCodigodel();
                if (findDelegaciones(id) == null) {
                    throw new NonexistentEntityException("The delegaciones with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(int id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Delegaciones delegaciones;
            try {
                delegaciones = em.getReference(Delegaciones.class, id);
                delegaciones.getCodigodel();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The delegaciones with id " + id + " no longer exists.", enfe);
            }
            em.remove(delegaciones);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Delegaciones> findDelegacionesEntities() {
        return findDelegacionesEntities(true, -1, -1);
    }

    public List<Delegaciones> findDelegacionesEntities(int maxResults, int firstResult) {
        return findDelegacionesEntities(false, maxResults, firstResult);
    }

    private List<Delegaciones> findDelegacionesEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Delegaciones.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Delegaciones findDelegaciones(int id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Delegaciones.class, id);
        } finally {
            em.close();
        }
    }

    public int getDelegacionesCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Delegaciones> rt = cq.from(Delegaciones.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    void destroy(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
