
package Persistencia;

import ClasesONG.Delegaciones;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ControladoraPersistencia {
 
    DelegacionesJpaController delegacionesJPA = new DelegacionesJpaController();
    
    public void crearDelegacion(Delegaciones delegaciones){
        
        try {
            delegacionesJPA.create(delegaciones);
        } catch (Exception ex) {
            Logger.getLogger(ControladoraPersistencia.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void modificarDelegacion(Delegaciones delegaciones){
        
        try {
            delegacionesJPA.edit(delegaciones);
        } catch (Exception ex) {
            Logger.getLogger(ControladoraPersistencia.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void eliminarDelegacion(int id){
        
        try {
            delegacionesJPA.destroy(id);
        } catch (Exception ex) {
            Logger.getLogger(ControladoraPersistencia.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public List<Delegaciones> listarDelegaciones(){
        
        List<Delegaciones> listarDelegaciones = new ArrayList<>();
        
        try {
            listarDelegaciones = delegacionesJPA.findDelegacionesEntities();
        } catch (Exception ex) {
            Logger.getLogger(ControladoraPersistencia.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return listarDelegaciones();
    }
    
    public Delegaciones listarDelegacion(int id){
        
        Delegaciones delegacion = new Delegaciones();
        
        try {
            delegacion = delegacionesJPA.findDelegaciones(id);
        } catch (Exception ex) {
            Logger.getLogger(ControladoraPersistencia.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return delegacion;
    }
    
}
