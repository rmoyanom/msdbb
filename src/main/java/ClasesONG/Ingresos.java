package ClasesONG;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "Ingresos")
@XmlAccessorType(XmlAccessType.FIELD)
public class Ingresos {
    @XmlAttribute(name = "idIngreso")
    protected long idIngreso;
    @XmlElement(name = "ingresosneto")
    protected float ingresosneto;
    @XmlElement(name = "ingresosbruto")
    protected float ingresosbrutos;
    @XmlElement(name = "ingresospriva")
    protected float ingresospriva;
    @XmlElement(name = "empresadonar")
    protected String empresadonar;
    @XmlElement(name = "numingresos")
    protected int numingresos;
    
    protected Delegaciones delegaciones;
    protected int codigodel;
    

    // M�todo constructor
    public Ingresos(long idIngreso, float ingresosneto, float ingresosbrutos, float ingrsospriva, String empresadonar, int numingresos) {
        this.idIngreso = idIngreso;
        this.ingresosneto = ingresosneto;
        this.ingresosbrutos = ingresosbrutos;
        this.ingresospriva = ingrsospriva;
        this.empresadonar = empresadonar;
        this.numingresos = numingresos;
    }

    public Ingresos(long idIngreso, float ingresosneto, float ingresosbrutos, float ingresospriva, String empresadonar, int numingresos, int codigodel) {
        this.idIngreso = idIngreso;
        this.ingresosneto = ingresosneto;
        this.ingresosbrutos = ingresosbrutos;
        this.ingresospriva = ingresospriva;
        this.empresadonar = empresadonar;
        this.numingresos = numingresos;
        this.delegaciones.setCodigodel(codigodel);
    }
        
    public Ingresos() {
    }

    // M�todos accesores
    public long getIdIngreso() {
        return idIngreso;
    }
    
    public void setIdIngreso(long idIngreso) {
        this.idIngreso = idIngreso;
    }
    
    public float getIngresosneto() {
        return ingresosneto;
    }

    public void setIngresosneto(float ingresosneto) {
        this.ingresosneto = ingresosneto;
    }

    public float getIngresosbrutos() {
        return ingresosbrutos;
    }

    public void setIngresosbrutos(float ingresosbrutos) {
        this.ingresosbrutos = ingresosbrutos;
    }

    public float getIngresospriva() {
        return ingresospriva;
    }

    public void setIngresospriva(float ingrsospriva) {
        this.ingresospriva = ingrsospriva;
    }

    public String getEmpresadonar() {
        return empresadonar;
    }

    public void setEmpresadonar(String empresadonar) {
        this.empresadonar = empresadonar;
    }

    public int getNumingresos() {
        return numingresos;
    }

    public void setNumingresos(int numingresos) {
        this.numingresos = numingresos;
    }

    public Delegaciones getDelegaciones() {
        return delegaciones;
    }

    public void setDelegaciones(Delegaciones delegaciones) {
        this.delegaciones = delegaciones;
    }

    public int getCodigodel() {
        return codigodel;
    }

    public void setCodigodel(int codigodel) {
        this.codigodel = codigodel;
    }

    @Override
    public String toString() {
        return "Ingresos{idIngreso=" + idIngreso +"ingresosneto=" + ingresosneto + ", ingresosbrutos=" 
        + ingresosbrutos + ", ingresospriva=" + ingresospriva + ", empresadonar="
        + empresadonar + ", numingresos=" + numingresos + "}";
    }
    
    
}
