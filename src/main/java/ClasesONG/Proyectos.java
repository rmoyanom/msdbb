package ClasesONG;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "Proyectos")
@XmlAccessorType(XmlAccessType.FIELD)
public class Proyectos {
    @XmlElement(name = "pais")
    protected String pais;
    @XmlElement(name = "localizacion")
    protected String localizacion;
    @XmlElement(name = "lineaccion")
    protected String lineaccion;
    @XmlElement(name = "sublineaccion")
    protected String sublineaccion;
    @XmlElement(name = "sociolocal")
    protected String sociolocal;
    @XmlElement(name = "financiador")
    protected String financiador;
    @XmlElement(name = "financiacionaport")
    protected float financiacionaport;
    @XmlAttribute(name = "codigoproyecto")
    protected int codigoproyecto;
    @XmlElement(name = "fechaini")
    protected String fechaini;
    @XmlElement(name = "fechafinal")
    protected String fechafinal;
    @XmlElement(name = "accionesrealizar")
    protected String accionesrealizar;
    @XmlElement(name = "personal")
    protected String personal;

    
    protected Delegaciones delegaciones;
    protected int codigodel; //Variable que se utiliza s�lo para listar los proyectos desde la clase DAO con el m�todo listar
    
    
// M�todo constructor
    public Proyectos() {
    }

    public Proyectos(String pais, String localizacion, String lineaccion, String sublineaccion, String sociolocal, String financiador, float financiacionaport,
            int codigoproyecto, String fechaini, String fechafinal, String accionesrealizar, String personal) {
        super();
        this.pais = pais;
        this.localizacion = localizacion;
        this.lineaccion = lineaccion;
        this.sublineaccion = sublineaccion;
        this.sociolocal = sociolocal;
        this.financiador = financiador;
        this.financiacionaport = financiacionaport;
        this.codigoproyecto = codigoproyecto;
        this.fechaini = fechaini;
        this.fechafinal = fechafinal;
        this.accionesrealizar = accionesrealizar;
        this.personal = personal;
    }

    public Proyectos(String pais, String localizacion, String lineaccion, String sublineaccion, String sociolocal, String financiador, float financiacionaport, 
            int codigoproyecto, String fechaini, String fechafinal, String accionesrealizar, String personal, int codigodel) {
        super();
        this.pais = pais;
        this.localizacion = localizacion;
        this.lineaccion = lineaccion;
        this.sublineaccion = sublineaccion;
        this.sociolocal = sociolocal;
        this.financiador = financiador;
        this.financiacionaport = financiacionaport;
        this.codigoproyecto = codigoproyecto;
        this.fechaini = fechaini;
        this.fechafinal = fechafinal;
        this.accionesrealizar = accionesrealizar;
        this.personal = personal;
        this.delegaciones.setCodigodel(codigodel);
    }
    
    
    
    // M�todos accesores
    public void setpais(String pais) {
        this.pais = pais;
    }

    public String getpais() {
        return pais;
    }

    public void setlocalizacion(String localizacion) {
        this.localizacion = localizacion;
    }

    public String getlocalizacion() {
        return localizacion;
    }

    public void setlineaccion(String lineaccion) {
        this.lineaccion = lineaccion;
    }

    public String getlineaccion() {
        return lineaccion;
    }

    public void setsublineaccion(String sublineaccion) {
        this.sublineaccion = sublineaccion;
    }

    public String getsublineaccion() {
        return sublineaccion;
    }

    public void setsociolocal(String sociolocal) {
        this.sociolocal = sociolocal;
    }

    public String getsociolocal() {
        return sociolocal;
    }

    public void setfinanciador(String financiador) {
        this.financiador = financiador;
    }

    public String getfinanciador() {
        return financiador;
    }

    public void setfinanciacionaport(float financiacionaport) {
        this.financiacionaport = financiacionaport;
    }

    public float getfinanciacionaport() {
        return financiacionaport;
    }

    public void setcodigoproyecto(int codigoproyecto) {
        this.codigoproyecto = codigoproyecto;
    }

    public int getcodigoproyecto() {
        return codigoproyecto;
    }

    public void setfechaini(String fechaini) {
        this.fechaini = fechaini;
    }

    public String getfechaini() {
        return fechaini;
    }

    public void setfechafinal(String fechafinal) {
        this.fechafinal = fechafinal;
    }

    public String getfechafinal() {
        return fechafinal;
    }

    public void setaccionesrealizar(String accionesrealizar) {
        this.accionesrealizar = accionesrealizar;
    }

    public String getaccionesrealizar() {
        return accionesrealizar;
    }

    public void setpersonal(String personal) {
        this.personal = personal;
    }

    public String getpersonal() {
        return personal;
    }

    public Delegaciones getDelegaciones() {
        return delegaciones;
    }

    public void setDelegaciones(Delegaciones delegaciones) {
        this.delegaciones = delegaciones;
    }

    public int getCodigodel() {
        return codigodel;
    }

    public void setCodigodel(int codigodel) {
        this.codigodel = codigodel;
    }

    @Override
    public String toString() {
        return "Proyectos {pais=" + pais + ", localizacion=" + localizacion + ", lineaccion= " + lineaccion
        + ", sublineaccion= " + sublineaccion + ", sociolocal=" + sociolocal + ", financiador " + financiador 
        + ", financiacionaport= " + financiacionaport + ", codigoproyecto= " + codigoproyecto 
        + ", fechaini= " + fechaini + ", fechafinal= " + fechafinal + ", accionesrealizar= "
        + accionesrealizar + ", personal= " + personal + "}";
        
    }

}
