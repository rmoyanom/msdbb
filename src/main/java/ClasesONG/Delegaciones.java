package ClasesONG;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.*;

@XmlRootElement(name = "Delegaciones")
@XmlAccessorType(XmlAccessType.FIELD)

@Entity
@Table (name = "delegaciones")
public class Delegaciones implements Serializable {

    @XmlElement(name = "nombre")
    @Column
    protected String nombre;
    @XmlElement(name = "proyectos")
    //@Column
    List<Proyectos> proyectos;
    @XmlAttribute(name = "codigodel")
    @Id
    @Column(name = "codigodel") //Se a�ade name en caso de que la columna en base de datos tenga distinto nombre a la variable en el programa
    protected int codigodel;
    @XmlElement(name = "director")
    @Column
    protected String director;
    @XmlElement(name = "numempleados")
    @Column
    protected int numempleados;
    @XmlElement(name = "ingresos")
    //@Column
    List<Ingresos> ingresos;
    
    
    protected ONG ong;
    protected int codong;

    // M�todos constructores
    public Delegaciones(String nombre, int codigodel, String director, int numempleados ) {
        super();
        this.nombre = nombre;
        this.codigodel = codigodel;
        this.director = director;
        this.numempleados = numempleados;
    }

    public Delegaciones() {
    }

    // M�todos accesores    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Proyectos> getproyecto() {
        return proyectos;
    }

    public void setProyectos(List<Proyectos> proyectos) {
        this.proyectos = proyectos;
    }

    void addp(Proyectos proyectos) {
        if (this.proyectos == null) {
            this.proyectos = new ArrayList<Proyectos>();
        }
        this.proyectos.add(proyectos);
    }

    public int getCodigodel() {
        return codigodel;
    }

    public void setCodigodel(int codigodel) {
        this.codigodel = codigodel;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public int getNumempleados() {
        return numempleados;
    }

    public void setNumempleados(int numempleados) {
        this.numempleados = numempleados;
    }

    public List<Ingresos> getingresos() {
        return ingresos;
    }

    public void setIngresos(List<Ingresos> ingresos) {
        this.ingresos = ingresos;
    }

    void addi(Ingresos ingresos) {
        if (this.ingresos == null) {
            this.ingresos = new ArrayList<Ingresos>();
        }
        this.ingresos.add(ingresos);
    }
    
    public ONG getONG() {
        return ong;
    }

    public void setONG(ONG ong) {
        this.ong = ong;
    }

    public int getCodong() {
        return codong;
    }

    public void setCodong(int codong) {
        this.codong = codong;
    }

    @Override
    public String toString() {
        return "Delegaciones{" + "nombre=" + nombre + ", codigodel=" + codigodel + ", director="
                + director + ", numempleados=" + numempleados + '}';
    }

}
