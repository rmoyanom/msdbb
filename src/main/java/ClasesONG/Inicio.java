package ClasesONG;

import ClasesDAO.DAOFactorySQL;
import ClasesDAO.CargaDatos;
import ClasesDAO.Conexion;
import ClasesDAO.IongDAO;
import ClasesDAO.IproyectosDAO;
import ClasesDAO.IingresosDAO;
import ClasesDAO.IdelegacionesDAO;
import ClasesDAO.OngDAO;
import ClasesDaoyXML.DAOFactory;
import ClasesDaoyXML.ONGDAO;
import Persistencia.ControladoraPersistencia;
import java.util.List;
import javax.xml.bind.JAXBException;

/**
 *
 * @author RIKY
 */
public class Inicio {

    private static ONG ong;

    private static ONG inicarong() {
//
//        //A�adimos las delegaciones y la ong
        ONG ongp = new ONG();
//        ongp.setLocalizacion("Madrid");
//        ongp.setNombre("ONG grande");
//        ongp.setNumdelegaciones(5);
//        ongp.setNumempleados(140);
//        ongp.setcodong(12624);
//        Delegaciones delegaciones1;
//        Delegaciones delegaciones2;
//        delegaciones1 = new Delegaciones("delegacion1", 1242623, "director", 57);
//        delegaciones2 = new Delegaciones("delegacion2", 169373, "director2", 32);
//        ongp.addd(delegaciones1);
//        ongp.addd(delegaciones2);
//
//        //A�adimos los proyectos
//        Proyectos proyectos1;
//        proyectos1 = new Proyectos("pais", "localizacion", "lineaacion", "sublineac", "socio", "financiador", 120, 29457, "124", "125", "acciones", "personal");
//        Proyectos proyectos2;
//        proyectos2 = new Proyectos("paiwrgs", "localiegzacion", "lineaegacion", "sublineegac", "socegio", "financegiador", 1260, 2944557, "12fdg4", "12e5", "accierhones", "persojtrnal");
//
//        delegaciones1.addp(proyectos1);
//        delegaciones2.addp(proyectos2);
//
//        //A�adimos los ingrsos
//        Ingresos ingresos1;
//        Ingresos ingresos2;
//        ingresos1 = new Ingresos(15647, 36000, 50000, 952000, "alquinol", 15);
//        ingresos2 = new Ingresos(29874, 24000, 30000, 250000, "minguita", 8);
//
//        delegaciones1.addi(ingresos2);
//        delegaciones2.addi(ingresos1);
//
        return ongp;
    }

    /**
     * @param args the command line arguments
     * @throws javax.xml.bind.JAXBException
     * * @throws JAXBException
     */
    /*public static void main(String[] args) throws JAXBException, Exception {

        ong = inicarong();
//        DAOFactory XmlDAOFactory = DAOFactory.getDAOFactory();
//        ONGDAO ongdao = XmlDAOFactory.getONGDAO();
//        ongdao.guardarONGDAO(ong);
//        ongdao.verONGDAO();

        //DAOFactorySQL sqldao = DAOFactorySQL.getDAOFactorySQL();
        //IongDAO ongsql = sqldao.ONG();
        //ongsql.a�adir(ong);
        
        CargaDatos conexion = new CargaDatos(); //Objeto conexi�n para probar la conexi�n
        
        conexion.cargadatos();
        //conexion.Conectar();//Probamos la conexi�n
        
        Delegaciones delegaciones = new Delegaciones("Cantabria", 20, "Many", 20);
        
        
    
        ControladoraPersistencia controlador = new ControladoraPersistencia();
        
        //Crear delegaci�n
        controlador.crearDelegacion(delegaciones);
        
        //Eliminar delegaci�n
        controlador.eliminarDelegacion(20);
        
        //Modificar delegaci�n
        controlador.modificarDelegacion(delegaciones);
        
        //Listar delegaciones
        List<Delegaciones> delegacionesListadas = controlador.listarDelegaciones();
        
        for(Delegaciones del : delegacionesListadas){
            System.out.println("Nombre: "+del.getNombre()+" C�digo: "+del.getCodigodel()+" Director: "+del.getDirector()+" Empleados: "+del.getNumempleados());
        }
        
        //Listar delegaci�n
        Delegaciones delegacion = controlador.listarDelegacion(20);
        System.out.println("Nombre: "+delegacion.getNombre()+" C�digo: "+delegacion.getCodigodel()+" Director: "+delegacion.getDirector()+" Empleados: "+delegacion.getNumempleados());
        

    }*/
    
    public void SiLaInterfazNoVa(){
        
        Delegaciones delegaciones = new Delegaciones();
        ControladoraPersistencia controlador = new ControladoraPersistencia();
        
        //Crear delegaci�n
        controlador.crearDelegacion(delegaciones);
        
        //Eliminar delegaci�n
        controlador.eliminarDelegacion(20);
        
        //Modificar delegaci�n
        controlador.modificarDelegacion(delegaciones);
        
        //Listar delegaciones
        List<Delegaciones> delegacionesListadas = controlador.listarDelegaciones();
        
        for(Delegaciones del : delegacionesListadas){
            System.out.println("Nombre: "+del.getNombre()+" C�digo: "+del.getCodigodel()+" Director: "+del.getDirector()+" Empleados: "+del.getNumempleados());
        }
        
        //Listar delegaci�n
        Delegaciones delegacion = controlador.listarDelegacion(20);
        System.out.println("Nombre: "+delegacion.getNombre()+" C�digo: "+delegacion.getCodigodel()+" Director: "+delegacion.getDirector()+" Empleados: "+delegacion.getNumempleados());
        
    }

}
