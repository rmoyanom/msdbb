package ClasesONG;

import java.util.ArrayList;
import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement(name = "ONG")
@XmlAccessorType(XmlAccessType.FIELD)
public class ONG {

    @XmlAttribute(name = "codong")
    protected int codong;
    @XmlElement(name = "delegaciones")
    List<Delegaciones> delegaciones;
    @XmlElement(name = "nombre")
    protected String nombre;
    @XmlElement(name = "numdelegaciones")
    protected int numdelegaciones;
    @XmlElement(name = "localizacion")
    protected String localizacion;
    @XmlElement(name = "numempleados")
    protected int numempleados;

    // M�todo constructor
    public ONG(String nombre, int numdelegaciones, String localizacion, int numempleados, int codong) {
        super();
        this.nombre = nombre;
        this.numdelegaciones = numdelegaciones;
        this.localizacion = localizacion;
        this.numempleados = numempleados;
        this.codong = codong;
    }

    public ONG() {
    }

    // M�todos accesores
    public List<Delegaciones> getdelegaciones() {
        return delegaciones;
    }

    public void setDelegaciones(List<Delegaciones> delegaciones) {
        this.delegaciones = delegaciones;
    }

    void addd(Delegaciones delegaciones) {
        if (this.delegaciones == null) {
            this.delegaciones = new ArrayList<Delegaciones>();
        }
        this.delegaciones.add(delegaciones);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getNumdelegaciones() {
        return numdelegaciones;
    }

    public void setNumdelegaciones(int numdelegaciones) {
        this.numdelegaciones = numdelegaciones;
    }

    public int getcodong() {
        return codong;
    }

    public void setcodong(int codong) {
        this.codong = codong;
    }

    public String getLocalizacion() {
        return localizacion;
    }

    public void setLocalizacion(String localizacion) {
        this.localizacion = localizacion;
    }

    public int getNumempleados() {
        return numempleados;
    }

    public void setNumempleados(int numempleados) {
        this.numempleados = numempleados;
    }

    @Override
    public String toString() {
        return "ONG {nombre=" + nombre + ", numdelegaciones="
                + numdelegaciones + ", localizacion=" + localizacion + ", numempleados=" + numempleados + "}";
    }

}
