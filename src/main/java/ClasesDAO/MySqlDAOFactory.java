/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClasesDAO;

/**
 *
 * @author RIKY
 */        //MySqlDAOFactory
public class MySqlDAOFactory extends DAOFactorySQL{

    @Override
    public IproyectosDAO proyectos(){
        return new ProyectosDAO();
    }
    
    @Override
    public IongDAO ONG(){
        return new OngDAO();
    }
    
    @Override
    public IingresosDAO ingresos(){
        return new IngresosDAO();
    }
    
    @Override
    public IdelegacionesDAO delegaciones(){
        return new DelegacionesDAO();
    }

}
