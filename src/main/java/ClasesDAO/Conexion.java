
package ClasesDAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.*;

public class Conexion {
//    private final String url = "jdbc:mysql://localhost:3306;databasename=sys";
//    private final String JDBC = "com.mysql.jdbc.Driver";
//    
//    private final String usuario = "root";
//    private final String contraseña = "toor";
//    protected Connection conexion;
    
    //Declaración del driver de SQL Server
    //private static String JDBC = "com.microsoft.sqlserver.jdbc.SQLServerDriver"; //SQLServer
    private static String JDBC = "com.mysql.jdbc.Driver"; //MYSQL
    
    //Definición de las variables
    private static String usuario = "root";
    private static String contraseña = "destructor1997";

    
    //private static String url = "jdbc:mysql://"+host+":"+puerto+"/"+baseDatos+","+usuario+","+contraseña;
    private static String url = "jdbc:mysql://localhost:3306/msdb";
    protected Connection conexion;
    
    public void Conectar(){
        
        String mensaje = null;
        
        try {
            Class.forName(JDBC);
            conexion = DriverManager.getConnection(url,usuario,contraseña);
            
        } catch (Exception e) {
            mensaje = e.toString();
        }
        
        if (conexion != null){
            System.out.println("Conexion realizada");
        }
        else{
            System.out.println("Conexión no realizada "+mensaje);
        }
    }
    
    public void Desconexion() throws SQLException{
        
        if (conexion != null){
            if (!conexion.isClosed()){
                conexion.close();
            }
        }
        
    }
}
