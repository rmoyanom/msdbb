package ClasesDAO;

/**
 *
 * @author RIKY
 */
public abstract class DAOFactorySQL {
    
    public abstract IproyectosDAO proyectos();
    
    public abstract IongDAO ONG();
    
    public abstract IingresosDAO ingresos();
    
    public abstract IdelegacionesDAO delegaciones();
    
    public static DAOFactorySQL getDAOFactorySQL() {
        return new MySqlDAOFactory();
    }
}
