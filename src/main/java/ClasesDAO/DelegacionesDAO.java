
package ClasesDAO;

import ClasesONG.Delegaciones;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;


public class DelegacionesDAO extends Conexion implements IdelegacionesDAO{

    @Override
    public void registrar(Delegaciones delegaciones) throws Exception {
        String sql = "Insert into delegaciones (codong,nombre,director, numempleados) VALUES ('"+delegaciones.getCodigodel()+"','"+delegaciones.getNombre()+"','"+delegaciones.getDirector()+"','"+delegaciones.getNumempleados()+"')";
        
        try {
            this.Conectar();
            Statement st = conexion.createStatement();
            st.execute(sql);
        } catch (Exception e) {
            throw e;
        }finally{
            this.Desconexion();
        }
    }

    @Override
    public void modificar(Delegaciones delegaciones, int n) throws Exception {
        String SQL=null;
        String sqlproyectos = "select * from proyectos where id = "+delegaciones.getCodigodel();
        
        try {
            switch(n) {
            case 1:
                
                int codong = Integer.parseInt(JOptionPane.showInputDialog("Introduce codigo ong"));
                delegaciones.getONG().setcodong(codong);
                SQL = "Update delegaciones set codong = '"+delegaciones.getONG().getcodong()+"' where idIngreso = '"+delegaciones.getCodigodel()+"'";

                break;
            case 2:
                
                String nombre = JOptionPane.showInputDialog("Introduce nombre");
                delegaciones.setNombre(nombre);
                SQL = "Update delegaciones set nombre = '"+delegaciones.getNombre()+"' where idIngreso = '"+delegaciones.getCodigodel()+"'";

                break;
            case 3:
                
                String director = JOptionPane.showInputDialog("Introduce director");
                delegaciones.setDirector(director);
                SQL = "Update delegaciones set director = '"+delegaciones.getDirector()+"' where idIngreso = '"+delegaciones.getCodigodel()+"'";

                break;
            case 4:
                
                int numempleados = Integer.parseInt(JOptionPane.showInputDialog("Introduce numempleados"));
                delegaciones.setNumempleados(numempleados);
                SQL = "Update delegaciones set numempleados = '"+delegaciones.getNumempleados()+"' where idIngreso = '"+delegaciones.getCodigodel()+"'";

                break;
            default:
                System.out.println("No has introducido bien el numero"); //Si introducimos un num�ro diferente a los que salen por pantalla, sacamos este mensaje
                }
            if (n > 0 && n < 6){
                this.Conectar();
                Statement st = conexion.createStatement();
                st.execute(SQL);
                /*ResultSet rs = st.executeQuery(sqlproyectos); Hacer esto en caso de querer recuperar alg�n campo destinto de codigoproyecto para sacarlo por pantalla al usuario
                
                while (rs.next()) {                    
                    proyectos.setcodigoproyecto(rs.getInt("codigoproyecto"));
                }*/
                
                JOptionPane.showMessageDialog(null, "Se ha modificado la delegacion con el id "+delegaciones.getCodigodel());
            }
        } catch (Exception e) {
            throw e;
        }finally{
            this.Desconexion();
        }
    }

    @Override
    public void eliminar(Delegaciones delegaciones) throws Exception {
        String sql = "DELETE FROM delegaciones where id = '"+delegaciones.getCodigodel()+"'";
        
        try {
            this.Conectar();
            Statement st = conexion.createStatement();
            st.execute(sql);
        } catch (Exception e) {
            throw e;
        }finally{
            this.Desconexion();
        }
    }

    @Override
    public List<Delegaciones> Listar() throws Exception {
        List<Delegaciones> delegaciones = new ArrayList<>();
        String sql = "select * from delegaciones";
        
        try {
            this.Conectar();
            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery(sql);
            
            while (rs.next()) {                
                delegaciones.add(new Delegaciones(rs.getString("nombre"), rs.getInt("codigodel"), rs.getString("director"), rs.getInt("numempleados")));
            }
        } catch (Exception e) {
        }
        
        return delegaciones;
    }
    
}
