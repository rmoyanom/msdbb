
package ClasesDAO;

import ClasesONG.Ingresos;
import java.util.List;


public interface IingresosDAO {
    public void a�adir(Ingresos ingresos) throws Exception;
    public void modificar(Ingresos ingresos, int n) throws Exception;
    public void eliminar(Ingresos ingresos) throws Exception;
    public List<Ingresos> Listar() throws Exception;
}
