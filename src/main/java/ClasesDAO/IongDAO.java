
package ClasesDAO;

import ClasesONG.ONG;
import java.util.List;


public interface IongDAO {

    public void a�adir(ONG ong) throws Exception;
    public void modificar(ONG ong, int n) throws Exception;
    public void eliminar(ONG ong) throws Exception;
    public List<ONG> Listar() throws Exception;
    
}
