
package ClasesDAO;

import ClasesONG.Proyectos;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;


public class ProyectosDAO extends Conexion implements IproyectosDAO{

    @Override
    public void a�adir(Proyectos proyectos) throws Exception {
        String SQL = "insert into proyectos(codigoproyecto, codong, pais, localizacion, lineacion, sublineacion, sociolocal, financiador, financiacionaport, fechaini, fechafinal, accionesrealizar, personal) VALUES('"+proyectos.getcodigoproyecto()+"','"+proyectos.getDelegaciones().getCodigodel()+"','"+proyectos.getpais()+"','"+proyectos.getlocalizacion()+"','"+proyectos.getlineaccion()+"','"+proyectos.getsublineaccion()+"','"+proyectos.getsociolocal()+"','"+proyectos.getfinanciador()+"','"+proyectos.getfinanciacionaport()+"','"+proyectos.getfechaini()+"','"+proyectos.getfechafinal()+"','"+proyectos.getaccionesrealizar()+"','"+proyectos.getpersonal()+"')";
        try {
            this.Conectar();
            Statement st = conexion.createStatement();
            st.execute(SQL);
        } catch (Exception e) {
            throw e;
        }finally{
            this.Desconexion();
        }
    }

    @Override
    public void modificar(Proyectos proyectos, int n) throws Exception {
        
        String SQL=null;
        String sqlproyectos = "select * from proyectos where id = "+proyectos.getcodigoproyecto();
        
        
        try {
            switch(n) {
            case 1:
                
                int cong = Integer.parseInt(JOptionPane.showInputDialog("Introduce c�digo de ONG"));
                proyectos.getDelegaciones().setCodigodel(cong);
                SQL = "Update proyectos set codong = '"+proyectos.getDelegaciones().getCodigodel()+"' where codigoproyecto = '"+proyectos.getcodigoproyecto()+"'";

                break;
            case 2:
                
                String pais = JOptionPane.showInputDialog("Introduce pais");
                proyectos.setpais(pais);
                SQL = "Update proyectos set pais = '"+proyectos.getpais()+"' where codigoproyecto = '"+proyectos.getcodigoproyecto()+"'";

                break;
            case 3:
                
                String localizacion = JOptionPane.showInputDialog("Introduce localizacion");
                proyectos.setlocalizacion(localizacion);
                SQL = "Update proyectos set pais = '"+proyectos.getlocalizacion()+"' where codigoproyecto = '"+proyectos.getcodigoproyecto()+"'";

                break;
            case 4:
                
                String lineacion = JOptionPane.showInputDialog("Introduce lineaci�n");
                proyectos.setlineaccion(lineacion);
                SQL = "Update proyectos set pais = '"+proyectos.getlineaccion()+"' where codigoproyecto = '"+proyectos.getcodigoproyecto()+"'";

                break;
            case 5:
                
                String sublineacion = JOptionPane.showInputDialog("Introduce sublineaci�n");
                proyectos.setsublineaccion(sublineacion);
                SQL = "Update proyectos set pais = '"+proyectos.getsublineaccion()+"' where codigoproyecto = '"+proyectos.getcodigoproyecto()+"'";

                break;
            case 6:
                
                String sociolocal = JOptionPane.showInputDialog("Introduce sociolocal");
                proyectos.setsociolocal(sociolocal);
                SQL = "Update proyectos set pais = '"+proyectos.getsociolocal()+"' where codigoproyecto = '"+proyectos.getcodigoproyecto()+"'";

                break;
            case 7:
                
                String financiador = JOptionPane.showInputDialog("Introduce financiador");
                proyectos.setfinanciador(financiador);
                SQL = "Update proyectos set pais = '"+proyectos.getfinanciador()+"' where codigoproyecto = '"+proyectos.getcodigoproyecto()+"'";

                break;
            case 8:
                
                float financiacionaport = Integer.parseInt(JOptionPane.showInputDialog("Introduce financiaci�n"));
                proyectos.setfinanciacionaport(financiacionaport);
                SQL = "Update proyectos set pais = '"+proyectos.getfinanciacionaport()+"' where codigoproyecto = '"+proyectos.getcodigoproyecto()+"'";

                break;
            case 9:
                
                String fechaini = JOptionPane.showInputDialog("Introduce fecha inicial");
                proyectos.setfechaini(fechaini);
                SQL = "Update proyectos set pais = '"+proyectos.getfechaini()+"' where codigoproyecto = '"+proyectos.getcodigoproyecto()+"'";

                break;
            case 10:
                
                String fechafinal = JOptionPane.showInputDialog("Introduce fecha final");
                proyectos.setfechafinal(fechafinal);
                SQL = "Update proyectos set pais = '"+proyectos.getfechafinal()+"' where codigoproyecto = '"+proyectos.getcodigoproyecto()+"'";

                break;
            case 11:
                
                String accionesrealizar = JOptionPane.showInputDialog("Introduce acciones a realizar");
                proyectos.setaccionesrealizar(accionesrealizar);
                SQL = "Update proyectos set pais = '"+proyectos.getaccionesrealizar()+"' where codigoproyecto = '"+proyectos.getcodigoproyecto()+"'";

                break;
            case 12:
                
                String personal = JOptionPane.showInputDialog("Introduce acciones a realizar");
                proyectos.setpersonal(personal);
                SQL = "Update proyectos set pais = '"+proyectos.getpersonal()+"' where codigoproyecto = '"+proyectos.getcodigoproyecto()+"'";

                break;
            default:
                System.out.println("No has introducido bien el numero"); //Si introducimos un num�ro diferente a los que salen por pantalla, sacamos este mensaje
                }
            if (n > 0 && n < 12){
                this.Conectar();
                Statement st = conexion.createStatement();
                st.execute(SQL);
                /*ResultSet rs = st.executeQuery(sqlproyectos); Hacer esto en caso de querer recuperar alg�n campo destinto de codigoproyecto para sacarlo por pantalla al usuario
                
                while (rs.next()) {                    
                    proyectos.setcodigoproyecto(rs.getInt("codigoproyecto"));
                }*/
                
                JOptionPane.showMessageDialog(null, "Se ha modificado el proyecto con el id "+proyectos.getcodigoproyecto());
            }
        } catch (Exception e) {
            throw e;
        }finally{
            this.Desconexion();
        }
    }

    @Override
    public void eliminar(Proyectos proyectos) throws Exception {
        String SQL = "delete from proyectos where codigoproyecto = '"+proyectos.getcodigoproyecto()+"'";
        
        try {
            this.Conectar();
            Statement st = conexion.createStatement();
            st.execute(SQL);
        } catch (Exception e) {
            throw e;
        }finally{
            this.Desconexion();
        }
    }

    @Override
    public List<Proyectos> Listar() throws Exception {
        List<Proyectos> proyectos = new ArrayList<>();
        String SQL = "select * from proyectos";
        try {
            this.Conectar();
            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery(SQL);
            
            while (rs.next()) {
                proyectos.add(new Proyectos(rs.getString("pais"), rs.getString("localizacion"), rs.getString("lineaccion"), rs.getString("sublineacion"), rs.getString("sociolocal"), rs.getString("financiador"), rs.getFloat("financiacionaport"), rs.getInt("codigoproyecto"), rs.getString("fechaini"), rs.getString("fechafinal"), rs.getString("accionesrealizar"), rs.getString("personal"), rs.getInt("codigodel")));
                //rs.getInt("codigoproyecto"), rs.getInt("codong"), rs.getString("pais"), rs.getString("localizacion"), rs.getString("lineaccion"), rs.getString("sublineacion"), rs.getString("sociolocal"), rs.getString("financiador"), rs.getString("financiacionaport"), rs.getString("fechaini"), rs.getString("fechafinal"), rs.getString("accionesrealizar"), rs.getString("personal"));
                
            }
        } catch (Exception e) {
        }finally{
            this.Desconexion();
        }
        
        return proyectos;
    }
    
}
