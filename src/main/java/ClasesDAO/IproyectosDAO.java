
package ClasesDAO;

import ClasesONG.Proyectos;
import java.util.List;


public interface IproyectosDAO {
    public void a�adir(Proyectos proyectos) throws Exception;
    public void modificar(Proyectos proyectos, int n) throws Exception;
    public void eliminar(Proyectos proyectos) throws Exception;
    public List<Proyectos> Listar() throws Exception;
}
