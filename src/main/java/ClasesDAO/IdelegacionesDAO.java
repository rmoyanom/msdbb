
package ClasesDAO;

import ClasesONG.Delegaciones;
import java.util.List;


public interface IdelegacionesDAO {
    public void registrar(Delegaciones delegaciones) throws Exception;
    public void modificar(Delegaciones delegaciones,int n) throws Exception;
    public void eliminar(Delegaciones delegaciones) throws Exception;
    public List<Delegaciones> Listar() throws Exception;
}
