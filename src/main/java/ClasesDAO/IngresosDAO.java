
package ClasesDAO;

import ClasesONG.Ingresos;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;


public class IngresosDAO extends Conexion implements IingresosDAO{

    @Override
    public void a�adir(Ingresos ingresos) throws Exception {
        String SQL = "insert into ingresos(ingresosneto, ingresosbruto, ingresospriva, empadronar, numingresos, codigodel) VALUES('"+ingresos.getIngresosneto()+"','"+ingresos.getIngresosbrutos()+"','"+ingresos.getIngresospriva()+"','"+ingresos.getEmpresadonar()+"','"+ingresos.getNumingresos()+"','"+ingresos.getDelegaciones().getCodigodel()+"')";
        try {
            this.Conectar();
            Statement st = conexion.createStatement();
            st.execute(SQL);
        } catch (Exception e) {
            throw e;
        }finally{
            this.Desconexion();
        }
    }

    @Override
    public void modificar(Ingresos ingresos, int n) throws Exception {
        String SQL=null;
        String sqlproyectos = "select * from proyectos where id = "+ingresos.getIdIngreso();
        
        
        try {
            switch(n) {
            case 1:
                
                float ingresosneto = Integer.parseInt(JOptionPane.showInputDialog("Introduce ingresos neto"));
                ingresos.setIngresosneto(ingresosneto);
                SQL = "Update ingresos set ingresosneto = '"+ingresos.getIngresosneto()+"' where idIngreso = '"+ingresos.getIdIngreso()+"'";

                break;
            case 2:
                
                float ingresosbruto = Integer.parseInt(JOptionPane.showInputDialog("Introduce ingresos bruto"));
                ingresos.setIngresosbrutos(ingresosbruto);
                SQL = "Update ingresos set ingresosbruto = '"+ingresos.getIngresosbrutos()+"' where idIngreso = '"+ingresos.getIdIngreso()+"'";

                break;
            case 3:
                
                float ingresospriva = Integer.parseInt(JOptionPane.showInputDialog("Introduce ingresos privados"));
                ingresos.setIngresospriva(ingresospriva);
                SQL = "Update ingresos set ingresospriva = '"+ingresos.getIngresospriva()+"' where idIngreso = '"+ingresos.getIdIngreso()+"'";

                break;
            case 4:
                
                String empadronar = JOptionPane.showInputDialog("Introduce empresa");
                ingresos.setEmpresadonar(empadronar);
                SQL = "Update proyectos set empadronar = '"+ingresos.getEmpresadonar()+"' where codigoproyecto = '"+ingresos.getIdIngreso()+"'";

                break;
            case 5:
                
                int numingresos = Integer.parseInt(JOptionPane.showInputDialog("Introduce ingresos"));
                ingresos.setNumingresos(numingresos);
                SQL = "Update ingresos set numingresos = '"+ingresos.getNumingresos()+"' where idIngreso = '"+ingresos.getIdIngreso()+"'";

                break;
            case 6:
                
                int codigodel = Integer.parseInt(JOptionPane.showInputDialog("Introduce codigo de delegaci�n"));
                ingresos.getDelegaciones().setCodigodel(codigodel);
                SQL = "Update ingresos set codigodel = '"+ingresos.getDelegaciones().getCodigodel()+"' where idIngreso = '"+ingresos.getIdIngreso()+"'";

                break;
            default:
                System.out.println("No has introducido bien el numero"); //Si introducimos un num�ro diferente a los que salen por pantalla, sacamos este mensaje
                }
            if (n > 0 && n < 6){
                this.Conectar();
                Statement st = conexion.createStatement();
                st.execute(SQL);
                /*ResultSet rs = st.executeQuery(sqlproyectos); Hacer esto en caso de querer recuperar alg�n campo destinto de codigoproyecto para sacarlo por pantalla al usuario
                
                while (rs.next()) {                    
                    proyectos.setcodigoproyecto(rs.getInt("codigoproyecto"));
                }*/
                
                JOptionPane.showMessageDialog(null, "Se ha modificado el ingreso con el id "+ingresos.getIdIngreso());
            }
        } catch (Exception e) {
            throw e;
        }finally{
            this.Desconexion();
        }
    }

    @Override
    public void eliminar(Ingresos ingresos) throws Exception {
        String SQL = "delete from ingresos where idIngreso = '"+ingresos.getIdIngreso()+"'";
        
        try {
            this.Conectar();
            Statement st = conexion.createStatement();
            st.execute(SQL);
        } catch (Exception e) {
            throw e;
        }finally{
            this.Desconexion();
        }
    }

    @Override
    public List<Ingresos> Listar() throws Exception {
        List<Ingresos> ingresos = new ArrayList<>();
        String SQL = "select * from ingresos";
        try {
            this.Conectar();
            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery(SQL);
            
            while (rs.next()) {
                
                ingresos.add(new Ingresos(rs.getLong("idIngreso"), rs.getFloat("ingresosneto"), rs.getFloat("ingresosbruto"), rs.getFloat("ingresospriva"), rs.getString("empadronar"), rs.getInt("numingresos"), rs.getInt("codigodel")));
                
            }
        } catch (Exception e) {
        }finally{
            this.Desconexion();
        }
        
        return ingresos;
    }
    
}
