
package ClasesDAO;

import ClasesONG.ONG;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

public class OngDAO extends Conexion implements IongDAO{

    @Override
    public void a�adir(ONG ong) throws Exception {
        String SQL = "Insert int ong (nombre, numdelegaciones, localizacion,numempleados) values ('"+ong.getNombre()+"','"+ong.getNumdelegaciones()+"','"+ong.getLocalizacion()+"','"+ong.getNumempleados()+"')";
        
        try {
            this.Conectar();
            Statement st = conexion.createStatement();
            st.execute(SQL);
        } catch (Exception e) {
            throw e;
        }finally{
            this.Desconexion();
        }
    }

    @Override
    public void modificar(ONG ong, int n) throws Exception {
        String SQL=null;
        String sqlproyectos = "select * from proyectos where id = "+ong.getcodong();
        
        try {
            switch(n) {
            case 1:
                
                String nombre = JOptionPane.showInputDialog("Introduce nombre");
                ong.setNombre(nombre);
                SQL = "Update ong set nombre = '"+ong.getNombre()+"' where idIngreso = '"+ong.getcodong()+"'";

                break;
            case 2:
                
                int numdelegaciones = Integer.parseInt(JOptionPane.showInputDialog("Introduce numdelegaciones"));
                ong.setNumdelegaciones(numdelegaciones);
                SQL = "Update ong set numdelegaciones = '"+ong.getNumdelegaciones()+"' where idIngreso = '"+ong.getcodong()+"'";

                break;
            case 3:
                
                String localizacion = JOptionPane.showInputDialog("Introduce localizacion");
                ong.setLocalizacion(localizacion);
                SQL = "Update ong set localizacion = '"+ong.getLocalizacion()+"' where idIngreso = '"+ong.getcodong()+"'";

                break;
            case 4:
                
                int numempleados = Integer.parseInt(JOptionPane.showInputDialog("Introduce numempleados"));
                ong.setNumempleados(numempleados);
                SQL = "Update ong set numempleados = '"+ong.getNumempleados()+"' where idIngreso = '"+ong.getcodong()+"'";

                break;
            default:
                System.out.println("No has introducido bien el numero"); //Si introducimos un num�ro diferente a los que salen por pantalla, sacamos este mensaje
                }
            if (n > 0 && n < 6){
                this.Conectar();
                Statement st = conexion.createStatement();
                st.execute(SQL);
                /*ResultSet rs = st.executeQuery(sqlproyectos); Hacer esto en caso de querer recuperar alg�n campo destinto de codigoproyecto para sacarlo por pantalla al usuario
                
                while (rs.next()) {                    
                    proyectos.setcodigoproyecto(rs.getInt("codigoproyecto"));
                }*/
                
                JOptionPane.showMessageDialog(null, "Se ha modificado la ong con el id "+ong.getcodong());
            }
        } catch (Exception e) {
            throw e;
        }finally{
            this.Desconexion();
        }
        
    }

    @Override
    public void eliminar(ONG ong) throws Exception {
        String SQL = "delete from ong where codong = '"+ong.getcodong()+"'";
        
        try {
            this.Conectar();
            Statement st = conexion.createStatement();
            st.execute(SQL);
        } catch (Exception e) {
            throw e;
        }finally{
            this.Desconexion();
        }
    }

    @Override
    public List<ONG> Listar() throws Exception {
        List<ONG> ong = new ArrayList<>();
        String SQL = "select * from ong";
        try {
            this.Conectar();
            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery(SQL);
            
            while (rs.next()) {
                ong.add(new ONG(rs.getString("nombre"),rs.getInt("numdelegaciones"),rs.getString("localizacion"),rs.getInt("numempleados"),rs.getInt("codong")));
                
            }
        } catch (Exception e) {
        }finally{
            this.Desconexion();
        }
        
        return ong;
    }
    
}
