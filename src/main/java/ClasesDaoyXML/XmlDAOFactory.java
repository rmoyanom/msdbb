package ClasesDaoyXML;

/**
 *
 * @author RIKY
 */

import javax.xml.bind.JAXBException;

public class XmlDAOFactory extends DAOFactory {

    @Override
    public ProyectosDAO getProyectosDAO() throws JAXBException {
            return new ProyectosDAO();
    }

    @Override
    public DelegacionesDao getDelegacionesDao() throws JAXBException {
        return new DelegacionesDao();
    }

    @Override
    public IngresosDAO getIngresosDAO() throws JAXBException {
        return new IngresosDAO();
    }

    @Override
    public ONGDAO getONGDAO() throws JAXBException {
        return new ONGDAO();
    }
}
