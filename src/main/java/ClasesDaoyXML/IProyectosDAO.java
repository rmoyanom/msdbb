package ClasesDaoyXML;

/**
 *
 * @author RIKY
 */
import java.util.List;
import ClasesONG.Proyectos;

public interface IProyectosDAO {

    public List<Proyectos> listarProyectosDAO();

    public Proyectos guardarProyectosDAO();
}
