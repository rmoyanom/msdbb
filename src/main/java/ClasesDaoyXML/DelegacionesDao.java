/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClasesDaoyXML;

import ClasesONG.ONG;
import ClasesONG.Delegaciones;
import java.io.File;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author RIKY
 */
public class DelegacionesDao {

    private JAXBContext jaxbContext = null;
    private String nombreFichero = null;

    public DelegacionesDao() throws JAXBException {
        this.jaxbContext = JAXBContext.newInstance(Delegaciones.class);
        this.nombreFichero = "Delegaciones.xml";
    }

    public void guardarDelegacionesDAO(Delegaciones delegaciones) throws JAXBException {
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(delegaciones, new File(nombreFichero));
        System.out.println();
        System.out.println("Se ha escrito el fichero " + nombreFichero + " con el siguiente contenido:");
        System.out.println();
        marshaller.marshal(delegaciones, System.out);
    }

    public Delegaciones listarDelegacionesDAO() throws JAXBException {
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        ONG delegaciones = (ONG) unmarshaller.unmarshal(new File(nombreFichero));

        System.out.println();
        System.out.println("Estos son las delegaciones contenidos en el fichero " + nombreFichero);

        delegaciones.getdelegaciones().stream().map(delegacion -> {
            System.out.println("---");
            return delegacion;
        }).map(delegacion -> {
            System.out.println("Codigo delegacion : \t" + delegacion.getCodigodel());
            return delegacion;
        }).forEachOrdered(delegacion -> {
            System.out.println("Nombre delegacion : \t" + delegacion.getNombre());
        });
        /*for(Ingresos ingresos : ingresos.getingresos()){
        System.out.println("Cantidad aportada : \t" + ingresos.getIngresosbrutos());
        System.out.println("Entidad aportadora : \t" + ingresos.getEmpresadonar());
        }*/
        return null;
    }
}
