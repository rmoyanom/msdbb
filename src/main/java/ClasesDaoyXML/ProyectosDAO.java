package ClasesDaoyXML;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import ClasesONG.Proyectos;
import java.io.File;
import ClasesONG.Delegaciones;

public final class ProyectosDAO {

    private JAXBContext jaxbContext = null;
    private String nombreFichero = null;

    public ProyectosDAO() throws JAXBException {
        this.jaxbContext = JAXBContext.newInstance(Proyectos.class);
        this.nombreFichero = "Proyectos.xml";
    }

    public void guardarProyectosDAO(Proyectos proyectos) throws JAXBException {
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(proyectos, new File(nombreFichero));
        System.out.println();
        System.out.println("Se ha escrito el fichero " + nombreFichero + " con el siguiente contenido:");
        System.out.println();
        marshaller.marshal(proyectos, System.out);
    }

    public Proyectos listarProyectosDAO() throws JAXBException {
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        Delegaciones proyectos = (Delegaciones) unmarshaller.unmarshal(new File(nombreFichero));

        System.out.println();
        System.out.println("Estos son los proyectos contenidos en el fichero " + nombreFichero);

        proyectos.getproyecto().stream().map(proyecto -> {
            System.out.println("---");
            return proyecto;
        }).map(proyecto -> {
            System.out.println("Codigo del proyecto: \t" + proyecto.getcodigoproyecto());
            return proyecto;
        }).forEachOrdered(proyecto -> {
            System.out.println("Pais del proyecto: \t" + proyecto.getpais());
        });
        /*for(Proyectos proyecto : proyectos.getproyecto()){
        System.out.println("Codigo del proyecto: \t" + proyecto.getcodigoproyecto());
        System.out.println("Pais del proyecto: \t" + proyecto.getpais());
        }*/
        return null;
    }
}
