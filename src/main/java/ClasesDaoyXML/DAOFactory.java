package ClasesDaoyXML;

/**
 *
 * @author RIKY
 */

import javax.xml.bind.JAXBException;

public abstract class DAOFactory {

    public abstract ProyectosDAO getProyectosDAO() throws JAXBException;

    public abstract DelegacionesDao getDelegacionesDao() throws JAXBException;

    public abstract IngresosDAO getIngresosDAO() throws JAXBException;

    public abstract ONGDAO getONGDAO() throws JAXBException;
    
    public static DAOFactory getDAOFactory() {
        return new XmlDAOFactory();
    }
}

