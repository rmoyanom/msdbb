/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClasesDaoyXML;

import ClasesONG.Delegaciones;
import ClasesONG.Ingresos;
import ClasesONG.Proyectos;
import java.io.File;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author RIKY
 */
public class IngresosDAO {

    private JAXBContext jaxbContext = null;
    private String nombreFichero = null;

    public IngresosDAO() throws JAXBException {
        this.jaxbContext = JAXBContext.newInstance(Ingresos.class);
        this.nombreFichero = "Ingresos.xml";
    }

    public void guardarIngresosDAO(Ingresos ingresos) throws JAXBException {
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(ingresos, new File(nombreFichero));
        System.out.println();
        System.out.println("Se ha escrito el fichero " + nombreFichero + " con el siguiente contenido:");
        System.out.println();
        marshaller.marshal(ingresos, System.out);
    }

    public Ingresos listarIngresosDAO() throws JAXBException {
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        Delegaciones ingresos = (Delegaciones) unmarshaller.unmarshal(new File(nombreFichero));

        System.out.println();
        System.out.println("Estos son los ingresos contenidos en el fichero " + nombreFichero);

        ingresos.getingresos().stream().map(ingreso -> {
            System.out.println("---");
            return ingreso;
        }).map(ingreso -> {
            System.out.println("Cantidad aportada : \t" + ingreso.getIngresosbrutos());
            return ingreso;
        }).forEachOrdered(ingreso -> {
            System.out.println("Entidad aportadora : \t" + ingreso.getEmpresadonar());
        });
        /*for(Ingresos ingresos : ingresos.getingresos()){
        System.out.println("Cantidad aportada : \t" + ingresos.getIngresosbrutos());
        System.out.println("Entidad aportadora : \t" + ingresos.getEmpresadonar());
        }*/
        return null;
    }

}
