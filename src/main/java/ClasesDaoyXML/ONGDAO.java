package ClasesDaoyXML;

import ClasesONG.ONG;
import java.io.File;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author RIKY
 */
public class ONGDAO {

    private JAXBContext jaxbContext = null;
    private String nombreFichero = null;

    public ONGDAO() throws JAXBException {
        this.jaxbContext = JAXBContext.newInstance(ONG.class);
        this.nombreFichero = "ONG.xml";
    }

    public void guardarONGDAO(ONG ong) throws JAXBException {
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(ong, new File(nombreFichero));
        System.out.println();
        System.out.println("Se ha escrito el fichero " + nombreFichero + " con el siguiente contenido:");
        System.out.println();
        marshaller.marshal(ong, System.out);
    }

    public void verONGDAO() throws JAXBException {
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        ONG ong = (ONG) unmarshaller.unmarshal(new File(nombreFichero));

        System.out.println();
        System.out.println("Estos son los ingresos contenidos en el fichero " + nombreFichero);

        System.out.println("Codigo ong :" + ong.getcodong());
        System.out.println("Nombre ong :" + ong.getNombre());
    }

}
