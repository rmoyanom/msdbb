package ClasesDaoyXML;

import ClasesONG.Delegaciones;
import java.util.List;

/*
 *
 * @author RIKY
 */
public interface IDelegacionesDAO {

    public List<Delegaciones> listarDelegacionesDAO();

    public Delegaciones guardarDelegacionesDAO();
}
