package ClasesDaoyXML;

import ClasesONG.Ingresos;
import java.util.List;

/**
 *
 * @author RIKY
 */
public interface IIngresosDAO {

    public List<Ingresos> listarIngresosDAO();

    public Ingresos guardarIngresosDAO();
}
